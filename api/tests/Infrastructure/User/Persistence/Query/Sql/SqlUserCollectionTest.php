<?php


namespace App\Tests\Infrastructure\User\Persistence\Query\Sql;


use App\Infrastructure\Core\Persistence\Builders\User\User\UserBuilder;
use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\User\User\UserFixture;
use App\Infrastructure\Core\Security\JWT\UserIdentity;
use App\Infrastructure\User\Persistence\Query\Sql\SqlUserCollection;
use App\Infrastructure\User\Persistence\Query\UserCollectionInterface;
use App\Tests\FixtureAwareTestCase;

class SqlUserCollectionTest extends FixtureAwareTestCase
{
    private UserCollectionInterface $userCollection;

    protected function setUp(): void
    {
        parent::setUp();
        $this->addFixture(new UserFixture());
        $this->executeFixtures();

        $entityManager = self::$container->get('doctrine')->getManager();
        $this->userCollection = new SqlUserCollection($entityManager);
    }

    public function testOneByIdentity()
    {
        $identity = new UserIdentity(
            UserBuilder::USER_ID,
            'test@test.com',
            'password'
        );

        $user = $this->userCollection->oneByIdentity($identity);

        $this->assertEquals($identity->getUsername(), $user->email);
    }
}