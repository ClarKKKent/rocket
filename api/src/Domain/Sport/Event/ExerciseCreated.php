<?php


namespace App\Domain\Sport\Event;


use App\Domain\Sport\Entity\Exercise;
use Symfony\Contracts\EventDispatcher\Event;

final class ExerciseCreated extends Event
{
    private Exercise $exercise;

    public function __construct(Exercise $exercise)
    {
        $this->exercise = $exercise;
    }

    public function getExercise(): Exercise
    {
        return $this->exercise;
    }
}