<?php


namespace App\Application\Sport\Exercise\Command\CreateExercise;


use App\Application\Core\CommandInterface;
use Symfony\Component\Validator\Constraints as Assert;

final class CreateExerciseCommand implements CommandInterface
{
    public string $title;

    public string $description;

    public function __construct(string $title, string $description)
    {
        $this->title = $title;
        $this->description = $description;
    }
}