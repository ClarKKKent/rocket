<?php


namespace App\Tests\Domain\Sport\ValueObject;


use App\Domain\Sport\ValueObject\ExerciseId;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class ExerciseIdTest extends TestCase
{
    public function testSuccess(): void
    {
        $exerciseId = ExerciseId::fromString($value = Uuid::uuid4()->toString());

        $this->assertEquals($value, $exerciseId->toString());
    }
}