<?php


namespace App\Tests\Infrastructure\Sport\Persistence\Store;


use App\Domain\Sport\Entity\Exercise;
use App\Domain\Sport\Repository\ExerciseRepositoryInterface;
use App\Domain\Sport\ValueObject\ExerciseId;
use App\Infrastructure\Core\Persistence\Builders\Sport\Exercise\ExerciseBuilder;
use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\Sport\Exercise\ExerciseFixture;
use App\Infrastructure\Sport\Persistence\Store\Sql\SqlExerciseRepository;
use App\Tests\FixtureAwareTestCase;
use Doctrine\ORM\EntityManagerInterface;

class SqlExerciseRepositoryTest extends FixtureAwareTestCase
{
    private EntityManagerInterface $entityManager;

    private ExerciseRepositoryInterface $exerciseRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->exerciseRepository = new SqlExerciseRepository($this->entityManager);
    }

    public function testSaveSuccess()
    {
        $exercise = Exercise::create(
          ExerciseId::fromString(ExerciseBuilder::TEST_ID),
          'title',
          'description'
        );

        $this->exerciseRepository->save($exercise);

        /** @var Exercise $existingExercise */
        $existingExercise = $this->entityManager
            ->getRepository(Exercise::class)
            ->find(ExerciseBuilder::TEST_ID);

        $this->assertEquals($existingExercise->getId()->toString(), $exercise->getId()->toString());
    }

    public function testDeleteSuccess()
    {
        $exercise = ExerciseBuilder::any();

        $this->exerciseRepository->save($exercise);
        $this->exerciseRepository->delete($exercise);

        $existingExercise = $this->entityManager
            ->getRepository(Exercise::class)
            ->find(ExerciseId::fromString(ExerciseBuilder::TEST_ID));

        $this->assertNull($existingExercise);
    }
}