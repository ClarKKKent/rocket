<?php


namespace App\Application\Sport\Exercise\Query\GetExercise;


use App\Application\Core\QueryInterface;

final class GetExerciseQuery implements QueryInterface
{
    public string $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }
}