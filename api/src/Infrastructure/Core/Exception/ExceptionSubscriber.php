<?php


namespace App\Infrastructure\Core\Exception;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class ExceptionSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => [
                ['notifyException', 0],
                ['logException', 10]
            ]
        ];
    }

    public function notifyException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        $data = [
            'title' => \str_replace('\\', '.', \get_class($exception)),
            'detail' => $exception->getMessage(),
            'code' => $exception->getCode(),
            'meta' => [
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
                'message' => $exception->getMessage(),
                'trace' => $exception->getTrace(),
                'traceString' => $exception->getTraceAsString(),
            ],
        ];
        $response = new JsonResponse();
        $response->setData($data);
        $response->setStatusCode(400);

        if ($exception instanceof ValidationException) {
            $errors = [];
            foreach ($exception->getViolations() as $violation) {
                $errors[$violation->getPropertyPath()] = $violation->getMessage();
            }
            $response->setStatusCode(422);
            $data = [
                'message' => $exception->getMessage(),
                'errors' => $errors,
                'code' => $exception->getCode(),
                'status' => $response->getStatusCode()
            ];
            $response->setData($data);
        }

        if ($exception instanceof ModelNotFoundException) {
            $response->setStatusCode(404);
            $data = [
                'title' => \str_replace('\\', '.', \get_class($exception)),
                'detail' => $exception->getMessage(),
                'code' => $exception->getCode(),
                'status' => $response->getStatusCode()
            ];
            $response->setData($data);
        }

        if ($exception instanceof AuthenticationException) {
            $response->setStatusCode(401);
            $data = [
                'title' => \str_replace('\\', '.', \get_class($exception)),
                'detail' => $exception->getMessage(),
                'code' => $exception->getCode(),
                'status' => $response->getStatusCode()
            ];
            $response->setData($data);
        }

        $event->setResponse($response);
    }

    public function logException(ExceptionEvent $event)
    {

    }
}