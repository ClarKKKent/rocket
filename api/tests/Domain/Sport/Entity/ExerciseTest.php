<?php


namespace App\Tests\Domain\Sport\Entity;


use App\Domain\Sport\Entity\Exercise;
use App\Domain\Sport\ValueObject\ExerciseId;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class ExerciseTest extends TestCase
{
    public function testSuccess()
    {
        $exercise = new Exercise(
            $id = ExerciseId::fromString(Uuid::uuid4()->toString()),
            $title ='title',
            $description = 'description'
        );

        $this->assertEquals($id, $exercise->getId());
        $this->assertEquals($title, $exercise->getTitle());
        $this->assertEquals($description, $exercise->getDescription());
    }

    public function testUpdateSuccess()
    {
        $exercise = new Exercise(
            $id = ExerciseId::fromString(Uuid::uuid4()->toString()),
            'title',
            'description'
        );

        $exercise->update(
            $newTitle = 'new title',
            $newDescription = 'new description'
        );

        $this->assertEquals($newTitle, $exercise->getTitle());
        $this->assertEquals($newDescription, $exercise->getDescription());
    }
}