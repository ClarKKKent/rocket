<?php


namespace App\Domain\User\ValueObject;


use App\Domain\Core\ValueObject\IdInterface;
use App\Domain\Core\ValueObject\ValueObjectInterface;
use Webmozart\Assert\Assert;

class UserId implements IdInterface, ValueObjectInterface
{
    private string $value;

    private function __construct(string $value)
    {
        Assert::uuid($value);
        $this->value = $value;
    }

    public static function fromString(string $value): self
    {
        return new self(
            $value
        );
    }

    public function toString(): string
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return $this->value;
    }
}