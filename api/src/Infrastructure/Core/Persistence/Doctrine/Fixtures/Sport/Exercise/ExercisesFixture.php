<?php


namespace App\Infrastructure\Core\Persistence\Doctrine\Fixtures\Sport\Exercise;


use App\Domain\Sport\Entity\Exercise;
use App\Domain\Sport\ValueObject\ExerciseId;
use App\Infrastructure\Core\Persistence\Builders\User\User\UserBuilder;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;

class ExercisesFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user = UserBuilder::any();

        for ($i = 1; $i <=3; $i++) {
            $exercise = Exercise::create(
                ExerciseId::fromString(Uuid::uuid4()->toString()),
                'Exercise ' . $i,
                'description'
            );
            $user->attachExercise($exercise);
        }

        $manager->persist($user);
        $manager->flush();
    }
}