<?php


namespace App\Infrastructure\User\Persistence\Store\Sql;


use App\Domain\User\Entity\User;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Domain\User\ValueObject\Email;
use App\Infrastructure\Core\Exception\ModelNotFoundException;
use App\Infrastructure\Core\Persistence\Doctrine\Repository\AbstractSqlRepository;
use Symfony\Component\Security\Core\User\UserInterface;

final class SqlUserRepository extends AbstractSqlRepository implements UserRepositoryInterface
{
    protected function getClass(): string
    {
        return User::class;
    }

    public function existByEmail(Email $email): bool
    {
        return $this->entityManager->createQueryBuilder()
            ->select('COUNT(u.id)')
            ->from(User::class, 'u')
            ->where('u.email = :email')
            ->setParameter(':email', $email->toString())
            ->getQuery()->getSingleScalarResult()>0;
    }

    public function oneByIdentity(UserInterface $identity): User
    {
        $result = $this->entityManager->createQueryBuilder()
            ->select('u')
            ->from(User::class, 'u')
            ->where('u.email = :email')
            ->setParameter(':email', $identity->getUsername())
            ->getQuery()->getSingleResult();

        if (!$result) {
            throw new ModelNotFoundException('User not found.');
        }

        return $result;
    }
}