<?php


namespace App\Domain\User\Entity;


use App\Domain\Core\Entity\EntityInterface;
use App\Domain\Sport\Entity\Exercise;
use App\Domain\User\Specification\UniqueEmailSpecificationInterface;
use App\Domain\User\ValueObject\Email;
use App\Domain\User\ValueObject\UserId;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;


class User implements EntityInterface
{
    private UserId $id;

    private Email $email;

    private string $passwordHash;

    private Collection $exercises;

    public function __construct(UserId $id, Email $email, string $passwordHash)
    {
        $this->id = $id;
        $this->email = $email;
        $this->passwordHash = $passwordHash;
        $this->exercises = new ArrayCollection();
    }

    public static function create(
        UserId $id,
        Email $email,
        string $passwordHash,
        UniqueEmailSpecificationInterface $specification
    ): self {
        $specification->isSatisfiedBy($email);

        return new self(
            $id,
            $email,
            $passwordHash
        );
    }

    public function attachExercise(Exercise $exercise): self
    {
        $this->exercises->add($exercise);

        $exercise->attachUser($this);

        return $this;
    }

    public function getId(): UserId
    {
        return $this->id;
    }

    public function getEmail(): ?Email
    {
        return $this->email;
    }

    public function getPasswordHash(): string
    {
        return $this->passwordHash;
    }
}