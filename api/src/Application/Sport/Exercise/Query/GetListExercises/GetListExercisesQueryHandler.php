<?php


namespace App\Application\Sport\Exercise\Query\GetListExercises;


use App\Application\Core\PaginatedCollection;
use App\Application\Core\QueryHandlerInterface;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Infrastructure\Core\Security\IdentityFetcherInterface;
use App\Infrastructure\Sport\Persistence\Query\ExerciseCollectionInterface;

final class GetListExercisesQueryHandler implements QueryHandlerInterface
{
    private ExerciseCollectionInterface $exerciseCollection;

    private UserRepositoryInterface $userRepository;

    private IdentityFetcherInterface $identityFetcher;

    public function __construct(
        ExerciseCollectionInterface $exerciseCollection,
        UserRepositoryInterface $userRepository,
        IdentityFetcherInterface $identityFetcher)
    {
        $this->exerciseCollection = $exerciseCollection;
        $this->userRepository = $userRepository;
        $this->identityFetcher = $identityFetcher;
    }

    public function __invoke(GetListExercisesQuery $query): PaginatedCollection
    {
        $user = $this->userRepository->oneByIdentity($this->identityFetcher->get());

        $query->userId = $user->getId()->toString();

        $exercises = $this->exerciseCollection->list($query);

        return new PaginatedCollection($exercises);
    }
}