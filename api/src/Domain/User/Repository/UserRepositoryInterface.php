<?php


namespace App\Domain\User\Repository;


use App\Domain\User\Entity\User;
use App\Domain\User\ValueObject\Email;
use Symfony\Component\Security\Core\User\UserInterface;

interface UserRepositoryInterface
{
    public function existByEmail(Email $email): bool;

    public function save(User $user): void;

    public function delete(User $user): void;

    public function oneByIdentity(UserInterface $identity): User;
}