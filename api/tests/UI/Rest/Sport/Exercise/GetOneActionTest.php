<?php


namespace App\Tests\UI\Rest\Sport\Exercise;


use App\Infrastructure\Core\Persistence\Builders\Sport\Exercise\ExerciseBuilder;
use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\Sport\Exercise\ExerciseFixture;
use App\Tests\UI\UITestCase;

class GetOneActionTest extends UITestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->addFixture(new ExerciseFixture());
        $this->executeFixtures();

        $this->setAuthenticatedClient();
    }

    public function testSuccess(): void
    {
        $this->request(
            'GET',
            '/api/v1/sport/exercises/' . ExerciseBuilder::TEST_ID
        );

        $statusCode = $this->client->getResponse()->getStatusCode();

        $this->assertEquals(200, $statusCode);
    }
}