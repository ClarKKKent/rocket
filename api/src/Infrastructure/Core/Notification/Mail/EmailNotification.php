<?php


namespace App\Infrastructure\Core\Notification\Mail;


use App\Infrastructure\Core\Async\AsyncEventInterface;

class EmailNotification implements AsyncEventInterface
{
    private const APP_EMAIL = 'rocket@test.com';

    private string $to;

    private ?string $from;

    private string $subject;

    private ?string $text;

    private ?string $html;

    public function __construct(
        string $to,
        string $subject,
        string $text = null,
        string $html = null,
        ?string $from = null)
    {
        $this->to = $to;
        $this->from = $from ?? self::APP_EMAIL;
        $this->subject = $subject;
        $this->text = $text;
        $this->html = $html;
    }

    public function getTo(): string
    {
        return $this->to;
    }

    public function getFrom(): string
    {
        return $this->from;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function getHtml(): ?string
    {
        return $this->html;
    }



}