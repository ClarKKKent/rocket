<?php


namespace App\Application\Sport\Exercise\Command\DeleteExercise;


use App\Application\Core\CommandHandlerInterface;
use App\Domain\Sport\Repository\ExerciseRepositoryInterface;
use App\Domain\Sport\ValueObject\ExerciseId;

final class DeleteExerciseCommandHandler implements CommandHandlerInterface
{
    private ExerciseRepositoryInterface $exerciseRepository;

    public function __construct(ExerciseRepositoryInterface $exerciseRepository)
    {
        $this->exerciseRepository = $exerciseRepository;
    }

    public function __invoke(DeleteExerciseCommand $message): void
    {
        $exercise = $this->exerciseRepository->oneById(ExerciseId::fromString($message->exerciseId));

        $this->exerciseRepository->delete($exercise);
    }
}