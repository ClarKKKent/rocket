<?php


namespace App\Infrastructure\Core\Security\JWT;


use App\Infrastructure\Core\Security\UserIdentityFactoryInterface;
use App\Infrastructure\User\Persistence\Query\UserCollectionInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

final class UserProvider implements UserProviderInterface
{
    private UserCollectionInterface $userCollection;

    private UserIdentityFactoryInterface $userIdentityFactory;

    public function __construct(
        UserCollectionInterface $userCollection,
        UserIdentityFactoryInterface $userIdentityFactory)
    {
        $this->userCollection = $userCollection;
        $this->userIdentityFactory = $userIdentityFactory;
    }

    public function loadUserByUsername(string $username): UserInterface
    {
        $user = $this->userCollection->oneByEmailForAuth($username);

        return $this->userIdentityFactory->createFromUser($user);
    }

    public function refreshUser(UserInterface $user): UserInterface
    {
        if (!$user instanceof UserIdentity) {
            throw new UnsupportedUserException('Invalid user class.' . \get_class($user));
        }
        $user = $this->userCollection->oneByEmailForAuth($user->getUsername());

        return $this->userIdentityFactory->createFromUser($user);
    }

    public function supportsClass(string $class): bool
    {
        return $class === UserIdentity::class;
    }
}