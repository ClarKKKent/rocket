<?php


namespace App\Tests\UI\Rest\Sport\Exercise;


use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\Sport\Exercise\ExercisesFixture;
use App\Tests\UI\UITestCase;

class GetListActionTest extends UITestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->addFixture(new ExercisesFixture());
        $this->executeFixtures();

        $this->setAnonymousAuthenticatedClient();
    }

    public function testSuccess()
    {
        $this->request(
            'GET',
            '/api/v1/sport/exercises',
            []
        );

        $statusCode = $this->client->getResponse()->getStatusCode();

        $this->assertEquals(200, $statusCode);
    }
}