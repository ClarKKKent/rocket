<?php


namespace App\Application\Sport\Exercise\Command\UpdateExercise;


use App\Application\Core\CommandHandlerInterface;
use App\Domain\Sport\Repository\ExerciseRepositoryInterface;
use App\Domain\Sport\ValueObject\ExerciseId;

final class UpdateExerciseCommandHandler implements CommandHandlerInterface
{
    private ExerciseRepositoryInterface $exerciseRepository;

    public function __construct(ExerciseRepositoryInterface $exerciseRepository)
    {
        $this->exerciseRepository = $exerciseRepository;
    }

    public function __invoke(UpdateExerciseCommand $message): void
    {
        $exercise = $this->exerciseRepository->oneById(ExerciseId::fromString($message->exerciseId));

        $exercise->update(
          $message->title,
          $message->description
        );

        $this->exerciseRepository->save($exercise);
    }
}