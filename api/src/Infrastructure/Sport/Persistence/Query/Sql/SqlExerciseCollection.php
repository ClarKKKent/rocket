<?php


namespace App\Infrastructure\Sport\Persistence\Query\Sql;


use App\Application\Sport\Exercise\Query\GetListExercises\GetListExercisesQuery;
use App\Domain\Sport\Entity\Exercise;
use App\Domain\User\Entity\User;
use App\Infrastructure\Core\Exception\ModelNotFoundException;
use App\Infrastructure\Core\Persistence\Sql\Query\AbstractSqlCollection;
use App\Infrastructure\Sport\Persistence\Query\ExerciseCollectionInterface;
use App\Infrastructure\Sport\Persistence\ReadModel\ExerciseView;

final class SqlExerciseCollection extends AbstractSqlCollection implements ExerciseCollectionInterface
{
    public function one(string $id): ExerciseView
    {
        $qb = $this->entityManager->getConnection()->createQueryBuilder()
            ->select('e.*')
            ->from($this->tableName(Exercise::class), 'e')
            ->leftJoin('e', $this->tableName(User::class), 'u', 'e.user_id = u.id')
            ->where('e.id = :exercise')
            ->setParameter(':exercise', $id);

        $query = $this->execute($qb);
        $query->setFetchMode(\PDO::FETCH_CLASS, ExerciseView::class);

        if (!$exercise = $query->fetch()) {
            throw new ModelNotFoundException('Exercise not found.');
        }

        return $exercise;
    }

    public function list(GetListExercisesQuery $message): array
    {
        $qb = $this->entityManager->getConnection()->createQueryBuilder()
            ->select('e.*')
            ->from($this->tableName(Exercise::class), 'e')
            ->leftJoin('e', $this->tableName(User::class), 'u', 'e.user_id = u.id')
            ->where('u.id = :user')
            ->setParameter(':user', $message->userId);

        $query = $this->execute($qb);
        $query->setFetchMode(\PDO::FETCH_CLASS, ExerciseView::class);

        return $query->fetchAll();
    }

    public function all(): array
    {
        // TODO: Implement all() method.
    }
}