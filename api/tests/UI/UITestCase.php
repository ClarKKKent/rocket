<?php


namespace App\Tests\UI;


use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\User\User\UserFixture;
use App\Tests\FixtureAware;


use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

abstract class UITestCase extends WebTestCase
{
    use FixtureAware;

    protected KernelBrowser $client;

    protected function setUp(): void
    {
        parent::setUp();
        static::bootKernel();
        self::ensureKernelShutdown();
        $this->client = static::createClient();
    }

    protected function setAuthenticatedClient()
    {
        $this->addFixture(new UserFixture());
        $this->executeFixtures();
        $this->client = $this->createAuthenticatedClient();
    }

    protected function setAnonymousAuthenticatedClient()
    {
        $this->client = $this->createAuthenticatedClient();
    }

    protected function request(string $method, string $url, array $data = [])
    {
        $this->client->request(
            $method,
            $url,
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($data)
        );
    }

    protected function createAuthenticatedClient(
        string $email = 'test@test.com', string $password = 'password'): KernelBrowser
    {
        $this->client->request(
            'POST',
            '/api/v1/auth/signin',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'email' => $email,
                'password' => $password
            ])
        );

        $data = json_decode($this->client->getResponse()->getContent(), true);

        $this->client->setServerParameter('HTTP_Authorization', sprintf('Bearer %s', $data['token']));

        return $this->client;
    }
}