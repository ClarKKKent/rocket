<?php


namespace App\Infrastructure\Sport\Persistence\Query;


use App\Application\Sport\Exercise\Query\GetListExercises\GetListExercisesQuery;
use App\Infrastructure\Sport\Persistence\ReadModel\ExerciseView;

interface ExerciseCollectionInterface
{
    public function one(string $id): ExerciseView;

    public function list(GetListExercisesQuery $message): array;

    public function all(): array;
}