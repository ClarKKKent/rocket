<?php


namespace App\Tests;



use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

abstract class FixtureAwareTestCase extends KernelTestCase
{
    use FixtureAware;

    protected function setUp(): void
    {
        static::bootKernel();
    }
}