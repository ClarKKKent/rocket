<?php


namespace App\Application\Core;


final class Collection
{
    private array $data;

    private array $resource;

    public function __construct(array $data)
    {
        $this->data = $data;
        $this->resource = array_map(fn($item) => new Item($item), $data);
    }

    public function resource(): array
    {
        return $this->resource;
    }
}