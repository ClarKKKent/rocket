<?php


namespace App\Application\Sport\Exercise\Command\CreateExercise;


use App\Application\Core\CommandHandlerInterface;
use App\Domain\Sport\Entity\Exercise;
use App\Domain\Sport\Repository\ExerciseRepositoryInterface;
use App\Domain\Sport\ValueObject\ExerciseId;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Infrastructure\Core\Security\IdentityFetcherInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Ramsey\Uuid\Uuid;

final class CreateExerciseCommandHandler implements CommandHandlerInterface
{
    private ExerciseRepositoryInterface $exerciseRepository;

    private UserRepositoryInterface $userRepository;

    private IdentityFetcherInterface $identityFetcher;

    private EventDispatcherInterface $event;

    public function __construct(
        ExerciseRepositoryInterface $exerciseRepository,
        UserRepositoryInterface $userRepository,
        IdentityFetcherInterface $identityFetcher,
        EventDispatcherInterface $event)
    {
        $this->exerciseRepository = $exerciseRepository;
        $this->userRepository = $userRepository;
        $this->identityFetcher = $identityFetcher;
        $this->event = $event;
    }

    public function __invoke(CreateExerciseCommand $message): void
    {
        $user = $this->userRepository->oneByIdentity($this->identityFetcher->get());

        $exercise = Exercise::create(
          ExerciseId::fromString(Uuid::uuid4()->toString()),
          $message->title,
          $message->description);

        $user->attachExercise($exercise);

        foreach ($exercise->releaseEvents() as $event) {
            $this->event->dispatch($event);
        }

        $this->exerciseRepository->save($exercise);
    }
}