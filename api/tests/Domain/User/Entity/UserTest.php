<?php


namespace App\Tests\Domain\User\Entity;


use App\Domain\User\Entity\User;
use App\Domain\User\ValueObject\Email;
use App\Domain\User\ValueObject\UserId;
use App\Infrastructure\Core\Security\Password\Bcrypt\BcryptPasswordHasher;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class UserTest extends TestCase
{
    public function testSuccess()
    {
        $user = new User(
            $id = UserId::fromString(Uuid::uuid4()->toString()),
            $email = Email::fromString('test@test.com'),
            (new BcryptPasswordHasher())->hash($passwordValue = 'password')
        );

        $this->assertEquals($id, $user->getId());
        $this->assertEquals($email, $user->getEmail());
        $this->assertNotEquals($passwordValue, $user->getPasswordHash());
    }
}