<?php


namespace App\Tests\Domain\User\ValueObject;


use App\Domain\User\ValueObject\Email;
use PHPUnit\Framework\TestCase;

class EmailTest extends TestCase
{
    public function testSuccess(): void
    {
        $email = Email::fromString($value = 'test@test.com');

        $this->assertEquals($value, $email->toString());
    }

    public function testIncorrect(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        Email::fromString($value = 'test');
    }

    public function testSuccessWithUpperValue()
    {
        $email = Email::fromString('Test@test.com');

        $this->assertEquals('test@test.com', $email->toString());
    }
}