<?php


namespace App\Tests\Infrastructure\User\Persistence\Store\Sql;


use App\Domain\User\Entity\User;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Infrastructure\Core\Persistence\Builders\User\User\UserBuilder;
use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\User\User\UserFixture;
use App\Infrastructure\User\Persistence\Store\Sql\SqlUserRepository;
use App\Tests\FixtureAwareTestCase;
use Doctrine\ORM\EntityManagerInterface;

class SqlUserRepositoryTest extends FixtureAwareTestCase
{
    private EntityManagerInterface $entityManager;

    private UserRepositoryInterface $userRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->userRepository = new SqlUserRepository($this->entityManager);
    }

    public function testSaveSuccess()
    {
        $user = UserBuilder::any();

        $this->userRepository->save($user);

        /** @var User $existingUser */
        $existingUser = $this->entityManager
            ->getRepository(User::class)
            ->find(UserBuilder::USER_ID);

        $this->assertEquals($existingUser->getId()->toString(), $user->getId()->toString());
    }

    public function testDelete()
    {
        $this->addFixture(new UserFixture());
        $this->executeFixtures();

        $user = $this->entityManager
            ->getRepository(User::class)
            ->find(UserBuilder::USER_ID);

        $this->userRepository->delete($user);

        $existingUser = $this->entityManager
            ->getRepository(User::class)
            ->find(UserBuilder::USER_ID);

        $this->assertNull($existingUser);
    }
}