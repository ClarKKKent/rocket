<?php


namespace App\Infrastructure\Core\Exception;


use Symfony\Component\Validator\ConstraintViolationListInterface;
use Throwable;

final class ValidationException extends \LogicException
{
    private ConstraintViolationListInterface $violations;

    /**
     * @psalm-suppress MissingReturnType
     * @param ConstraintViolationListInterface $violations
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(ConstraintViolationListInterface $violations, $message = "Your data is not valid", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->violations = $violations;
    }

    public function getViolations(): ConstraintViolationListInterface
    {
        return $this->violations;
    }
}