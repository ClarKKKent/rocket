<?php


namespace App\Infrastructure\Core\Persistence\Doctrine\Type;


use App\Domain\Sport\ValueObject\ExerciseId;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;

class ExerciseIdType extends GuidType
{
    public const NAME = 'exercise_id';

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        return $value instanceof ExerciseId ? $value->toString() : $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?ExerciseId
    {
       return !empty($value) ? ExerciseId::fromString($value) : null;
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}