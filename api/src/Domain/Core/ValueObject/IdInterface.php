<?php


namespace App\Domain\Core\ValueObject;


interface IdInterface
{
    public function toString(): ?string;
}