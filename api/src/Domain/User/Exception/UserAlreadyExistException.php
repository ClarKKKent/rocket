<?php


namespace App\Domain\User\Exception;


use Throwable;

final class UserAlreadyExistException extends \LogicException
{
    /**
     * @psalm-suppress MissingParamType
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "User already exist.", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}