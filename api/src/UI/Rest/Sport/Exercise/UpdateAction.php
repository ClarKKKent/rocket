<?php


namespace App\UI\Rest\Sport\Exercise;


use App\Application\Sport\Exercise\Command\UpdateExercise\UpdateExerciseCommand;
use App\Infrastructure\Core\UI\RestCQRSController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

final class UpdateAction extends RestCQRSController
{
    /**
     * @Route("/api/v1/sport/exercises/{id}", methods={"PUT"})
     *
     * @param string $id
     * @param Request $request
     * @return JsonResponse
     */
    public function __invoke(string $id, Request $request): JsonResponse
    {
        $command = new UpdateExerciseCommand(
            $id,
            $request->get('title'),
            $request->get('description')
        );

        $this->exec($command);

        return $this->noContent();
    }
}