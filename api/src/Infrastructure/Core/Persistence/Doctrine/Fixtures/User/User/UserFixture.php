<?php


namespace App\Infrastructure\Core\Persistence\Doctrine\Fixtures\User\User;


use App\Infrastructure\Core\Persistence\Builders\User\User\UserBuilder;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $user = UserBuilder::any();

        $manager->persist($user);
        $manager->flush();
    }
}