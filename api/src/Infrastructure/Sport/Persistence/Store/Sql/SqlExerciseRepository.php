<?php


namespace App\Infrastructure\Sport\Persistence\Store\Sql;


use App\Domain\Sport\Entity\Exercise;
use App\Domain\Sport\Repository\ExerciseRepositoryInterface;
use App\Domain\Sport\ValueObject\ExerciseId;
use App\Infrastructure\Core\Exception\ModelNotFoundException;
use App\Infrastructure\Core\Persistence\Doctrine\Repository\AbstractSqlRepository;

final class SqlExerciseRepository extends AbstractSqlRepository implements ExerciseRepositoryInterface
{
    protected function getClass(): string
    {
        return Exercise::class;
    }

    public function oneById(ExerciseId $id): Exercise
    {
        /** @var Exercise $exercise */
        if (!$exercise = $this->repository->find($id->toString())) {
            throw new ModelNotFoundException('Exercise not found.');
        };

        return $exercise;
    }
}