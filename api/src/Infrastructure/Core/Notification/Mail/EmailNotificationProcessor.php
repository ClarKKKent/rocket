<?php


namespace App\Infrastructure\Core\Notification\Mail;


use App\Infrastructure\Core\Async\AsyncEventProcessorInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

final class EmailNotificationProcessor implements AsyncEventProcessorInterface
{
    private MailerInterface $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function __invoke(EmailNotification $event)
    {
        $email = (new Email())
            ->from($event->getFrom())
            ->to($event->getTo())
            ->subject($event->getSubject())
            ->text($event->getText())
            ->html($event->getHtml());

        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {

        }
    }
}