<?php


namespace App\Infrastructure\Core\Security\JWT;


use App\Infrastructure\Core\Security\UserIdentityFactoryInterface;
use App\Infrastructure\User\Persistence\ReadModel\AuthView;
use Symfony\Component\Security\Core\User\UserInterface;

final class UserIdentityFactory implements UserIdentityFactoryInterface
{
    public function createFromUser(AuthView $user): UserInterface
    {
        return new UserIdentity(
            $user->id,
            $user->email,
            $user->password_hash
        );
    }
}