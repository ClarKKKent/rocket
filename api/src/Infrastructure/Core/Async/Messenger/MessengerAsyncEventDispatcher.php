<?php


namespace App\Infrastructure\Core\Async\Messenger;


use App\Infrastructure\Core\Async\AsyncEventDispatcherInterface;
use App\Infrastructure\Core\Async\AsyncEventInterface;
use Symfony\Component\Messenger\MessageBusInterface;

final class MessengerAsyncEventDispatcher implements AsyncEventDispatcherInterface
{
    private MessageBusInterface $eventBus;

    public function __construct(MessageBusInterface $eventBus)
    {
        $this->eventBus = $eventBus;
    }

    public function dispatch(AsyncEventInterface $event): void
    {
        $this->eventBus->dispatch($event);
    }
}