<?php


namespace App\Infrastructure\User\Specification;


use App\Domain\User\Exception\UserAlreadyExistException;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Domain\User\Specification\UniqueEmailSpecificationInterface;


final class UniqueEmailSpecification implements UniqueEmailSpecificationInterface
{
    private UserRepositoryInterface $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @template T
     * @psalm-param T $value
     * @return bool
     */
    public function isSatisfiedBy($value): bool
    {
        if ($this->userRepository->existByEmail($value)) {
            throw new UserAlreadyExistException();
        }
        return true;
    }
}