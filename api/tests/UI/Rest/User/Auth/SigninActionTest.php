<?php


namespace App\Tests\UI\Rest\User\Auth;


use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\User\User\UserFixture;
use App\Tests\UI\UITestCase;

class SigninActionTest extends UITestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->addFixture(new UserFixture());
        $this->executeFixtures();
    }

    public function testSuccess()
    {
        $this->client->request(
            'POST',
            '/api/v1/auth/signin',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'email' => 'test@test.com',
                'password' => 'password'
            ])
        );
        $statusCode = $this->client->getResponse()->getStatusCode();

        $this->assertEquals(200, $statusCode);
    }
}