<?php


namespace App\Infrastructure\Core\Async;


use Symfony\Component\Messenger\MessageBusInterface;

interface AsyncEventDispatcherInterface
{
    public function dispatch(AsyncEventInterface $event): void;
}