<?php


namespace App\Domain\User\Specification;

/**
 * @template T
 */
interface UniqueEmailSpecificationInterface
{
    /**
     * @param $value
     * @psalm-param T $value
     * @return bool
     */
    public function isSatisfiedBy($value): bool;
}