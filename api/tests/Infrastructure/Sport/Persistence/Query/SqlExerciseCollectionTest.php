<?php


namespace App\Tests\Infrastructure\Sport\Persistence\Query;


use App\Application\Sport\Exercise\Query\GetListExercises\GetListExercisesQuery;
use App\Infrastructure\Core\Persistence\Builders\Sport\Exercise\ExerciseBuilder;
use App\Infrastructure\Core\Persistence\Builders\User\User\UserBuilder;
use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\Sport\Exercise\ExerciseFixture;
use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\Sport\Exercise\ExercisesFixture;
use App\Infrastructure\Sport\Persistence\Query\ExerciseCollectionInterface;
use App\Infrastructure\Sport\Persistence\Query\Sql\SqlExerciseCollection;
use App\Tests\FixtureAwareTestCase;

class SqlExerciseCollectionTest extends FixtureAwareTestCase
{
    private ExerciseCollectionInterface $exerciseCollection;

    protected function setUp(): void
    {
        parent::setUp();

        $entityManager = self::$container->get('doctrine')->getManager();
        $this->exerciseCollection = new SqlExerciseCollection($entityManager);
    }

    public function testListSuccess()
    {
        $this->addFixture(new ExercisesFixture());
        $this->executeFixtures();

        $query = new GetListExercisesQuery();
        $query->userId = UserBuilder::USER_ID;

        $exercises = $this->exerciseCollection->list($query);

        $this->assertCount(3, $exercises);
    }

    public function testOneSuccess()
    {
        $this->addFixture(new ExerciseFixture());
        $this->executeFixtures();

        $exercise = $this->exerciseCollection->one(ExerciseBuilder::TEST_ID);

        $this->assertEquals(ExerciseBuilder::TEST_ID, $exercise->id);
    }
}