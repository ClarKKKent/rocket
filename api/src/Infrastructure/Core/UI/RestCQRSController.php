<?php

namespace App\Infrastructure\Core\UI;


use App\Application\Core\Collection;
use App\Application\Core\CommandInterface;
use App\Application\Core\Item;
use App\Application\Core\PaginatedCollection;
use App\Application\Core\QueryInterface;
use App\Infrastructure\Core\Bus\CommandBusInterface;
use App\Infrastructure\Core\Bus\QueryBusInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/** @psalm-suppress PropertyNotSetInConstructor */
class RestCQRSController extends RestController
{
    private CommandBusInterface $commandBus;

    private QueryBusInterface $queryBus;

    private JsonApiFormatter $formatter;

    public function __construct(
        CommandBusInterface $commandBus,
        QueryBusInterface $queryBus,
        JsonApiFormatter $formatter)
    {
        $this->commandBus = $commandBus;
        $this->queryBus = $queryBus;
        $this->formatter = $formatter;
        parent::__construct($formatter);
    }

    protected function exec(CommandInterface $command): void
    {
        $this->commandBus->handle($command);
    }

    /**
     * @param QueryInterface $query
     * @return Collection|Item|PaginatedCollection
     */
    protected function ask(QueryInterface $query)
    {
        return $this->queryBus->handle($query);
    }
}