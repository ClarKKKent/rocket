<?php


namespace App\UI\Rest\Sport\Exercise;


use App\Application\Sport\Exercise\Query\GetExercise\GetExerciseQuery;
use App\Infrastructure\Core\UI\RestCQRSController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

final class GetOneAction extends RestCQRSController
{
    /**
     * @Route("/api/v1/sport/exercises/{id}", methods={"GET"})
     *
     * @param string $id
     * @return JsonResponse
     */
    public function __invoke(string $id): JsonResponse
    {
        $query = new GetExerciseQuery($id);

        $result = $this->ask($query);

        return $this->ok($result);
    }
}