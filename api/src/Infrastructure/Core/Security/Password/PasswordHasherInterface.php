<?php


namespace App\Infrastructure\Core\Security\Password;


interface PasswordHasherInterface
{
    /**
     * @psalm-ignore-nullable-return
     * @param string $password
     * @return false|string|null
     */
    public function hash(string $password);

    /**
     * @param string $password
     * @param string $passwordHash
     * @return bool
     */
    public function verify(string $password, string $passwordHash): bool;
}