<?php


namespace App\Infrastructure\Core\Bus;


use App\Application\Core\Collection;
use App\Application\Core\Item;
use App\Application\Core\PaginatedCollection;
use App\Application\Core\QueryInterface;

interface QueryBusInterface
{
    /**
     * @param QueryInterface $query
     * @return Item|Collection|PaginatedCollection
     */
    public function handle(QueryInterface $query);
}