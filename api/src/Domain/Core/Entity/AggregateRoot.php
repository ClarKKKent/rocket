<?php


namespace App\Domain\Core\Entity;


abstract class AggregateRoot
{
    protected array $events = [];

    public function releaseEvents(): array
    {
        $events = $this->events;
        $this->events = [];

        return $events;
    }

    protected function recordEvent($event): void
    {
        $this->events[] = $event;
    }
}