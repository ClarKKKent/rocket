<?php


namespace App\Domain\Core\Entity;


use App\Domain\Core\ValueObject\IdInterface;

interface EntityInterface
{
    public function getId(): IdInterface;
}