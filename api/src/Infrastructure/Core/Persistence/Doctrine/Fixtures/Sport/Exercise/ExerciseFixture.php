<?php


namespace App\Infrastructure\Core\Persistence\Doctrine\Fixtures\Sport\Exercise;


use App\Infrastructure\Core\Persistence\Builders\Sport\Exercise\ExerciseBuilder;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

final class ExerciseFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $exercise = ExerciseBuilder::any();

        $manager->persist($exercise);
        $manager->flush();
    }
}