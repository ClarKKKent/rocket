<?php


namespace App\UI\Rest\Sport\Exercise;


use App\Application\Sport\Exercise\Query\GetListExercises\GetListExercisesQuery;
use App\Infrastructure\Core\UI\RestCQRSController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

final class GetListAction extends RestCQRSController
{
    /**
     * @Route("/api/v1/sport/exercises", methods={"GET"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function __invoke(Request $request): JsonResponse
    {
        $query = new GetListExercisesQuery(
            $request->query->get('page')
        );

        $result = $this->ask($query);

        return $this->okPaginatedCollection($result);
    }
}