<?php


namespace App\Infrastructure\Core\Persistence\Builders\User\User;


use App\Domain\User\Entity\User;
use App\Domain\User\ValueObject\Email;
use App\Domain\User\ValueObject\UserId;
use App\Infrastructure\Core\Security\Password\Bcrypt\BcryptPasswordHasher;

class UserBuilder
{
    public const USER_ID = 'd9b4c852-71d1-44fc-a552-4ca37c3063d9';

    public static function any(): User
    {
        return new User(
            UserId::fromString(self::USER_ID),
            Email::fromString('test@test.com'),
            (new BcryptPasswordHasher())->hash('password')
        );
    }
}