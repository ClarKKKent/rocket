<?php


namespace App\Domain\Sport\Repository;


use App\Domain\Sport\Entity\Exercise;
use App\Domain\Sport\ValueObject\ExerciseId;

interface ExerciseRepositoryInterface
{
    public function save(Exercise $exercise): void;

    public function delete(Exercise $exercise): void;

    public function oneById(ExerciseId $id): Exercise;
}