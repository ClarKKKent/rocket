<?php


namespace App\Infrastructure\Sport\Event;


use App\Domain\Sport\Event\ExerciseCreated;
use App\Domain\Sport\Repository\ExerciseRepositoryInterface;
use App\Infrastructure\Core\Async\AsyncEventDispatcherInterface;
use App\Infrastructure\Core\Notification\Mail\EmailNotification;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class ExerciseCreatedSubscriber implements EventSubscriberInterface
{
    private AsyncEventDispatcherInterface $asyncEvent;

    private ExerciseRepositoryInterface $exerciseRepository;

    public function __construct(
        AsyncEventDispatcherInterface $asyncEvent,
        ExerciseRepositoryInterface $exerciseRepository)
    {
        $this->asyncEvent = $asyncEvent;
        $this->exerciseRepository = $exerciseRepository;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ExerciseCreated::class => 'onExerciseCreated'
        ];
    }

    public function onExerciseCreated(ExerciseCreated $event): void
    {
        $this->asyncEvent->dispatch(new EmailNotification(
            $event->getExercise()->getUser()->getEmail()->toString(),
            'Rocket app',
            'Упражнение ' . $event->getExercise()->getTitle() . ' успешно создано')
        );
    }
}