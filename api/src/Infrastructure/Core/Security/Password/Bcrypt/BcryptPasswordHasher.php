<?php


namespace App\Infrastructure\Core\Security\Password\Bcrypt;


use App\Infrastructure\Core\Security\Password\PasswordHasherInterface;

final class BcryptPasswordHasher implements PasswordHasherInterface
{
    /**
     * @psalm-ignore-nullable-return
     * @param string $password
     * @return false|string|null
     */
    public function hash(string $password)
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }

    /**
     * @psalm-ignore-nullable-return
     * @param string $password
     * @param string $passwordHash
     * @return bool
     */
    public function verify(string $password, string $passwordHash): bool
    {
        return password_verify($password, $passwordHash);
    }
}