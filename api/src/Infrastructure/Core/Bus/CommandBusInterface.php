<?php


namespace App\Infrastructure\Core\Bus;


use App\Application\Core\CommandInterface;

interface CommandBusInterface
{
    public function handle(CommandInterface $command): void;
}