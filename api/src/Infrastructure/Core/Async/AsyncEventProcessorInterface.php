<?php


namespace App\Infrastructure\Core\Async;


use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

interface AsyncEventProcessorInterface extends MessageHandlerInterface
{

}