<?php


namespace App\Application\User\Auth\Command\SigninUser;


use App\Application\Core\CommandInterface;

final class SigninUserCommand implements CommandInterface
{
    public string $email;

    public string $password;

    public function __construct(string $email, string $password)
    {
        $this->email = $email;
        $this->password = $password;
    }
}