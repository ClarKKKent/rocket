<?php


namespace App\Tests\Domain\User\ValueObject;


use App\Domain\User\Entity\User;
use App\Domain\User\ValueObject\UserId;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class UserIdTest extends TestCase
{
    public function testSuccess(): void
    {
        $id = UserId::fromString($value = Uuid::uuid4()->toString());

        $this->assertEquals($value, $id->toString());
    }

    public function testIncorrect(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        UserId::fromString('1');
    }
}