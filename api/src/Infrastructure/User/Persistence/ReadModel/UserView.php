<?php


namespace App\Infrastructure\User\Persistence\ReadModel;


use App\Infrastructure\Core\Persistence\ReadModelInterface;

/** @psalm-suppress MissingConstructor */
final class UserView implements ReadModelInterface
{
    public ?string $id;

    public ?string $email;

    public ?string $password_hash;

    public function serialize(): array
    {
        return [
            'id' => $this->id,
            'email' => $this->email
        ];
    }
}