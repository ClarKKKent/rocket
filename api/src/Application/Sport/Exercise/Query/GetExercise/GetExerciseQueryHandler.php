<?php


namespace App\Application\Sport\Exercise\Query\GetExercise;


use App\Application\Core\CommandHandlerInterface;
use App\Application\Core\Item;
use App\Infrastructure\Sport\Persistence\Query\ExerciseCollectionInterface;

final class GetExerciseQueryHandler implements CommandHandlerInterface
{
    private ExerciseCollectionInterface $exerciseCollection;

    public function __construct(ExerciseCollectionInterface $exerciseCollection)
    {
        $this->exerciseCollection = $exerciseCollection;
    }

    public function __invoke(GetExerciseQuery $message): Item
    {
        $exercise = $this->exerciseCollection->one($message->id);

        return new Item($exercise);
    }
}