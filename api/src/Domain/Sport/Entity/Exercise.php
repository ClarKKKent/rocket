<?php


namespace App\Domain\Sport\Entity;


use App\Domain\Core\Entity\AggregateRoot;
use App\Domain\Core\Entity\EntityInterface;
use App\Domain\Sport\Event\ExerciseCreated;
use App\Domain\Sport\ValueObject\ExerciseId;
use App\Domain\User\Entity\User;

class Exercise extends AggregateRoot implements EntityInterface
{
    private ExerciseId $id;

    private string $title;

    private string $description;

    private ?User $user;

    public function __construct(ExerciseId $id, string $title, string $description)
    {
        $this->id = $id;
        $this->title = $title;
        $this->description = $description;
    }

    public static function create(ExerciseId $id, string $title, string $description): self
    {
        $exercise = new Exercise($id, $title, $description);
        $exercise->recordEvent(new ExerciseCreated($exercise));

        return $exercise;
    }

    public function update(string $title, string $description)
    {
        $this->title = $title;
        $this->description = $description;
    }

    public function attachUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getId(): ExerciseId
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function __toString(): string
    {
        return $this->title;
    }
}