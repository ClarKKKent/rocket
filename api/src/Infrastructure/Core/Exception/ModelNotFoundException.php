<?php


namespace App\Infrastructure\Core\Exception;


use Throwable;

final class ModelNotFoundException extends \LogicException
{
    /**
     * @psalm-suppress MissingParamType
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "Model not found.", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}