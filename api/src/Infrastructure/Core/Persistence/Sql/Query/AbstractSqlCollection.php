<?php


namespace App\Infrastructure\Core\Persistence\Sql\Query;


use Doctrine\DBAL\Driver\ResultStatement;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractSqlCollection
{
    protected EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    protected function execute(QueryBuilder $qb): ResultStatement
    {
        return $this->entityManager->getConnection()->executeQuery($qb->getSQL(), $qb->getParameters());
    }

    protected function tableName(string $className): string
    {
        return $this->entityManager->getClassMetadata($className)->getTableName();
    }


}