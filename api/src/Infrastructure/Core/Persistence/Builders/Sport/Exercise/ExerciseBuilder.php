<?php


namespace App\Infrastructure\Core\Persistence\Builders\Sport\Exercise;


use App\Domain\Sport\Entity\Exercise;
use App\Domain\Sport\ValueObject\ExerciseId;

final class ExerciseBuilder
{
    public const TEST_ID = '6636b20a-71a5-4bed-b889-fb69c5053a00';

    public static function any(): Exercise
    {
        return new Exercise(
            ExerciseId::fromString(self::TEST_ID),
            'title',
            'description'
        );
    }
}