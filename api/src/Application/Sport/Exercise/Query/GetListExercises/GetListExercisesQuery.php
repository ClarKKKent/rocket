<?php


namespace App\Application\Sport\Exercise\Query\GetListExercises;


use App\Application\Core\AbstractListQuery;
use App\Application\Core\QueryInterface;

final class GetListExercisesQuery extends AbstractListQuery implements QueryInterface
{
    public ?string $userId = null;

    public function __construct(?int $page = 1, ?int $limit = 10)
    {
        parent::__construct($page, $limit);
    }
}