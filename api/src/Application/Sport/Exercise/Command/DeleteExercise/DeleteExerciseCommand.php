<?php


namespace App\Application\Sport\Exercise\Command\DeleteExercise;


use App\Application\Core\CommandInterface;

final class DeleteExerciseCommand implements CommandInterface
{
    public string $exerciseId;

    public function __construct(string $exerciseId)
    {
        $this->exerciseId = $exerciseId;
    }
}