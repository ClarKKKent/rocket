<?php


namespace App\Tests\UI\Rest\Sport\Exercise;


use App\Infrastructure\Core\Persistence\Builders\Sport\Exercise\ExerciseBuilder;
use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\Sport\Exercise\ExerciseFixture;
use App\Tests\UI\UITestCase;

class DeleteActionTest extends UITestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->addFixture(new ExerciseFixture());
        $this->executeFixtures();

        $this->setAuthenticatedClient();
    }

    public function testSuccess()
    {
        $this->request(
            'DELETE',
            '/api/v1/sport/exercises/' . ExerciseBuilder::TEST_ID,
            [

            ]
        );

        $statusCode = $this->client->getResponse()->getStatusCode();

        $this->assertEquals(204, $statusCode);
    }
}